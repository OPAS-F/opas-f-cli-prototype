from setuptools import setup
import os

user_home_dir = os.path.join(os.path.expanduser('~'), '.opasf')
data_files = [(user_home_dir, ['opasf_cli/config.json'])]

setup(
    name='opasf_cli',
    version='0.1',
    packages=[
        'opasf_cli', 'opasf_cli.plugins', 'opasf_cli.plugins.nikto',
        'opasf_cli.plugins.nmap', 'opasf_cli.plugins.wapiti'
    ],
    dependency_links=['git+https://codeberg.org/OPAS-F/opas-f-python-api.git#egg=opasf_api-0.1.0'],
    install_requires=['pluginlib', 'opasf_api'],
    url='',
    license='GPLv3',
    author='cysec',
    author_email='',
    description='',
    scripts=['opasf_cli/opasf'],
    data_files=data_files
)
