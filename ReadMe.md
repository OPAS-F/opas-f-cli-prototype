# OPAS-F CLI prototype

`python setup.py install`


usage: opasf \<command>

e.g.:

`opasf nmap -v example.com`


# Built-In Plugins

- nmap
- nikto
- wapiti

# Issues
Issue Tracker can be found [here](https://codeberg.org/OPAS-F/opas-f-docs/issues)