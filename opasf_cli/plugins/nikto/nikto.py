import re
import logging
import uuid
import xml.etree.ElementTree as ET
import opasf_cli.plugins.baseplugin as baseplugin


class NiktoPlugin(baseplugin.BasePlugin):
    """ nikto plugin inspired by faraday ide plugin """
    regex = r'^(sudo nikto|nikto|sudo nikto\.pl|nikto\.pl|perl nikto\.pl|\.\/nikto\.pl|\.\/nikto).*?'
    rewrite_regex = re.compile(r"^.*(-output\s*[^\s]+).*$")
    output_file = uuid.uuid4().hex
    logger = logging.getLogger(__name__)
    _alias_ = 'nikto'
    _version_ = '0.1'

    def rewrite_command(self, orig_command):
        if self.needs_command_rewrite(orig_command):
            return re.sub(r"(^.*?nikto(\.pl)?)",
                          r"\1 -output %s -Format XML" % self.output_file,
                          orig_command)
        else:
            data = re.sub(" \-Format XML", "", orig_command)
            return re.sub(self.rewrite_regex.match(orig_command).group(1),
                          r"-output %s -Format XML" % self.output_file,
                          data)

    def needs_command_rewrite(self, orig_command):
        if self.rewrite_regex.match(orig_command):
            self.logger.debug("Command needs rewrite")
            return False
        return True

    def start(self, command):
        logging.info("Started nikto command")
        super(NiktoPlugin, self).start(command)
        new_command = self.rewrite_command(command)
        self.logger.debug("New Command is: %s", new_command)
        self.logger.debug("Command ID is: %s", self.command_id)
        return new_command

    def parse(self):
        root = ET.parse(self.output_file)
        host = self.get_host(root)
        hostname = self.get_hostname(root)
        service = self.get_service(root)
        host_resp = self.client.create_host(self.project, host.get('ip'), command_id=self.command_id)
        hostname_resp = self.client.create_hostname(hostname.get('name'), host_resp.get('id'), command_id=self.command_id)
        service_resp = self.client.create_service(
            host_resp.get('id'), service.get('name'), service.get('port'),
            service.get('protocol'), command_id=self.command_id, version=service.get('version'))
        for vuln in self.get_vulnerabilities(root):
            self.client.create_web_vulnerability(
                service_resp.get('id'), vuln.get('name'), vuln.get('severity'),
                hostname_resp.get('id'), vuln.get('path'), poc=vuln.get('poc'), description=vuln.get('description'),
                command_id=self.command_id)

    def get_host(self, root):
        details = self.get_details(root)
        ip = details.get('targetip')
        return {'ip': ip}

    def get_details(self, root):
        return root.findall('scandetails')[0]

    def get_hostname(self, root):
        details = self.get_details(root)
        name = details.get('targethostname')
        return {'name': name}

    def get_service(self, root):
        details = self.get_details(root)
        version = details.get('targetbanner')
        port = details.get('targetport')
        name = details.get('sitename').split(":")[0]
        return {'name': name, 'port': port, 'protocol': 'tcp', 'version': version}

    def get_vulnerabilities(self, root):
        vulns = []
        items = self.get_details(root).findall('item')
        for item in items:
            info = {
                'name': item.find('description').text.split(".")[0],
                'path': item.find('uri').text,
                'poc': item.find('namelink').text,
                'severity': 'Unclassified'
            }
            if len(item.find('description').text.split(".")) > 1:
                info['description'] = '.'.join(item.find('description').text.split(".")[1:])
            vulns.append(info)
        return vulns
