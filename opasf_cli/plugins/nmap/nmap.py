import re
import logging
import uuid
import xml.etree.ElementTree as ET
import opasf_cli.plugins.baseplugin as baseplugin


class NmapPlugin(baseplugin.BasePlugin):
    """ nmap plugin inspired by faraday ide plugin """
    regex = r'^(sudo nmap|nmap|\.\/nmap).*?'
    rewrite_regex = re.compile(r'^.*(-oX\s*[^\s]+)-*$')
    output_file = uuid.uuid4().hex
    logger = logging.getLogger(__name__)

    def start(self, command):
        super(NmapPlugin, self).start(command)
        if self.rewrite_regex.match(command) is None:
            new_command = re.sub(r'(^.*?nmap)', r'\1 -oX %s' % self.output_file, command)
        else:
            new_command = re.sub(self.rewrite_regex.group(1), r'-oX %s' % self.output_file, command)
        self.logger.debug("New Command is: %s", new_command)
        self.logger.debug("Command ID is: %s", self.command_id)
        return new_command

    def parse(self):
        root = ET.parse(self.output_file)
        hosts = self.get_hosts(root)
        for host in hosts:
            host_resp = self.client.create_host(
                self.project, host.find('address').get('addr'),
                status=host.find('status').get('state'),
                command_id=self.command_id
            )
            for hostname in self.get_hostnames(host):
                self.client.create_hostname(
                    hostname.get('name'), host_resp.get('id'),
                    command_id=self.command_id
                )
            for service in self.get_services(host):
                self.client.create_service(
                    host_resp.get('id'), service.find('service').get('name'),
                    service.get('portid'), service.get('protocol'),
                    status=service.find('state').get('state'),
                    command_id=self.command_id
                )

    def get_hosts(self, root):
        hosts = root.findall('host')
        return hosts

    def get_hostnames(self, host):
        return host.find('hostnames').findall('hostname')

    def get_services(self, host):
        return host.find('ports').findall('port')
