import re
import logging
import uuid
import socket
from urllib.parse import urlparse
import xml.etree.ElementTree as ET
import opasf_cli.plugins.baseplugin as baseplugin


class WapitiPlugin(baseplugin.BasePlugin):
    """ wapiti plugin inspired by faraday ide plugin """
    regex = r'^(python wapiti|wapiti|sudo wapiti|sudo wapiti\.py|wapiti\.py|' \
            r'python wapiti\.py|\.\/wapiti\.py|wapiti|\.\/wapiti|python wapiti|python \.\/wapiti).*?'
    rewrite_regex = re.compile(r"^.*(-oX\s*[^\s]+).*$")
    output_file = uuid.uuid4().hex
    logger = logging.getLogger(__name__)
    _alias_ = 'wapiti'
    _version_ = '0.1'

    def rewrite_command(self, orig_command):
        if self.needs_command_rewrite(orig_command):
            return "%s -o %s -f xml \n" % (orig_command, self.output_file)
        return orig_command

    def needs_command_rewrite(self, orig_command):
        if self.rewrite_regex.match(orig_command):
            self.logger.debug("Command needs rewrite")
            return False
        return True

    def start(self, command):
        logging.info("Started wapiti command")
        super(WapitiPlugin, self).start(command)
        new_command = self.rewrite_command(command)
        self.logger.debug("New Command is: %s", new_command)
        self.logger.debug("Command ID is: %s", self.command_id)
        return new_command

    def parse(self):
        root = ET.parse(self.output_file)
        host = self.get_host(root)
        hostname = self.get_hostname(root)
        service = self.get_service(root)
        host_resp = self.client.create_host(self.project, host.get('ip'), command_id=self.command_id)
        hostname_resp = self.client.create_hostname(hostname.get('name'), host_resp.get('id'), command_id=self.command_id)
        service_resp = self.client.create_service(
            host_resp.get('id'), service.get('name'), service.get('port'),
            service.get('protocol'), command_id=self.command_id, version=service.get('version'))
        for vuln in self.get_vulnerabilities(root):
            self.client.create_web_vulnerability(
                service_resp.get('id'), vuln.get('name'), vuln.get('severity'),
                hostname_resp.get('id'), vuln.get('path'), poc=vuln.get('poc'), description=vuln.get('description'),
                command_id=self.command_id)

    def get_host(self, root):
        hostname = root.find("report_infos").find("info[@name='target']").text
        ip = socket.gethostbyname(urlparse(hostname).hostname)
        return {'ip': ip}

    def get_hostname(self, root):
        host = root.find("report_infos").find("info[@name='target']").text
        name = urlparse(host).hostname
        return {'name': name}

    def get_service(self, root):
        host = root.find("report_infos").find("info[@name='target']").text
        parsed = urlparse(host)
        port = 80
        if parsed.port:
            port = parsed.port
        elif parsed.scheme == "https":
            port = 443
        name = parsed.scheme
        return {'name': name, 'port': port, 'protocol': 'tcp', 'version': None}

    def get_vulnerabilities(self, root):
        vulns = []
        items = root.find("vulnerabilities").findall("vulnerability")
        for item in items:
            if item.find("entries"):
                for entry in item.find('entries').findall("entry"):
                    print(str(entry))
                    print(entry.find("method").text)
                    print(entry.find("level").text)
                    print(entry.find('http_request').text)
                    level = int(entry.find("level").text)
                    severity = "Unclassified"
                    if level == 1:
                        severity = "High"
                    elif level == 2:
                        severity = "Medium"

                    info = {
                        'name': item.get('name'), 'path': entry.find('path').text,
                        'poc': entry.find('curl_command').text, 'severity': severity,
                        'description': entry.find('info').text,
                    }
                    vulns.append(info)
        return vulns


if __name__ == '__main__':
    root = ET.parse("test.xml")
    vulns = WapitiPlugin().get_vulnerabilities(root)
    print(vulns)