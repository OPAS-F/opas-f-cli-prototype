from datetime import datetime
import pluginlib


@pluginlib.Parent('tools')
class BasePlugin(object):

    regex = None
    command_id = None
    project = None
    client = None

    def start(self, command):
        resp_command = self.client.command_started(self.project, command)
        self.command_id = resp_command.get('id')

    def ended(self):
        self.client.command_ended(self.command_id, str(datetime.now()))
        self.parse()

    def init_plugin(self, client, project):
        self.client = client
        self.project = project

    @pluginlib.abstractmethod
    def parse(self):
        pass
